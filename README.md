
Rofi nerd font glyphs selector
==============================

Description
-----------

This is a prototype project. The goal of this project is to try conception to load nerd font glyphs in to the rofi

How it is work
--------------

Scripts work with next simple algorithm:
* Scan '/usr/lib/nerd-fonts-complete' path. All files found contains nerd font glyphs description
* All found glyphs concatenate to list and pass to rofi.
* Selected glyphs pass to xdotool or xsel based on rofi complete action

Dependepcies
------------

Next dependencies must be installed:
* python3 (yes! this is a python script)
* nerd-fonts-complete
* rofi
* xdotool - used to paste selected values to active window
* xsel - used to copy selected values to clipboard

References
----------

* Rofi nerd font glyphs plugin: [https://gitlab.com/tekillaz/rofi-nerd-font-glyphs-plugin](https://gitlab.com/tekillaz/rofi-nerd-font-glyphs-plugin)
* Nerd fonts: [https://nerdfonts.com/](https://nerdfonts.com/)

